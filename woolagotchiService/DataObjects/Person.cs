﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Mobile.Service;

namespace woolagotchiService.DataObjects
{
    public class Person : EntityData
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public string hashedPassword { get; set; }
        public string userSalt { get; set; }
        public int coins { get; set; }
        public int userID { get; set; }


    }
}
