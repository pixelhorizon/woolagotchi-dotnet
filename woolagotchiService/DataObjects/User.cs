﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace woolagotchiService.DataObjects
{
    public class User : EntityData
    {
        public string userName { get; set; }
        public string hashedPassword { get; set; }
        public string userSalt { get; set; }
        public int coins { get; set; }
        
    }
}